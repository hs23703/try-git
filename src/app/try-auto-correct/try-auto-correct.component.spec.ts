import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TryAutoCorrectComponent } from './try-auto-correct.component';

describe('TryAutoCorrectComponent', () => {
  let component: TryAutoCorrectComponent;
  let fixture: ComponentFixture<TryAutoCorrectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TryAutoCorrectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TryAutoCorrectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
