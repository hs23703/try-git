import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-try-auto-correct',
  templateUrl: './try-auto-correct.component.html',
  styleUrls: ['./try-auto-correct.component.css']
})
export class TryAutoCorrectComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  filteredBrands:string[];
  brands: string[] = ['Audi','BMW','Fiat','Ford','Honda','Jaguar','Mercedes','Renault','Volvo','VW'];
  brand:string;

  filterBrands(event) {
    this.filteredBrands = [];
    for(let i = 0; i < this.brands.length; i++) {
        let brand = this.brands[i];
        if (brand.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
            this.filteredBrands.push(brand);
        }
    }
}

}
