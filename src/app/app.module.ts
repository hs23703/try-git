import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AutoCompleteModule } from 'primeng/autocomplete';
import {DropdownModule} from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TryAutoCorrectComponent } from './try-auto-correct/try-auto-correct.component';
import { TryDropDownComponent } from './try-drop-down/try-drop-down.component';
import { TryFlexComponent } from './try-flex/try-flex.component';
import { TryFormComponent } from './try-form/try-form.component';

@NgModule({
  declarations: [
    AppComponent,
    TryAutoCorrectComponent,
    TryDropDownComponent,
    TryFlexComponent,
    TryFormComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AutoCompleteModule,
    FormsModule,
    DropdownModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
