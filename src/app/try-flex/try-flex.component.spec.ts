import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TryFlexComponent } from './try-flex.component';

describe('TryFlexComponent', () => {
  let component: TryFlexComponent;
  let fixture: ComponentFixture<TryFlexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TryFlexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TryFlexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
