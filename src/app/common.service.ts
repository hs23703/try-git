import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor() { 
    console.log("hello from common service");
    let el=document.getElementById("idCheck");
    console.log(el);
    let el1=document.getElementById("scriptCheck")
    console.log(el1);
    el.setAttribute("style","color:Red;")
    el.innerText="not hello";
    el1.setAttribute("src","abc");
  }

public nextDay():Date{
  let next=new Date();
  next.setDate(next.getDate()+1);
  return next;
}

}
