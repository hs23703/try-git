import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TryAutoCorrectComponent } from './try-auto-correct/try-auto-correct.component'
import { TryDropDownComponent } from './try-drop-down/try-drop-down.component';
import { TryFlexComponent } from './try-flex/try-flex.component';
import { TryFormComponent } from './try-form/try-form.component';


const routes: Routes = [
  {path:"auto-complete",component: TryAutoCorrectComponent },
  {path:"dropdown", component: TryDropDownComponent},
  {path:"flex", component: TryFlexComponent},
{path:'form', component:TryFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
