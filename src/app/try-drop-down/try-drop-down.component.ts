import { Component, OnInit } from '@angular/core';

interface City {
  name: string;
  code: string;
}

@Component({
  selector: 'app-try-drop-down',
  templateUrl: './try-drop-down.component.html',
  styleUrls: ['./try-drop-down.component.css']
})
export class TryDropDownComponent implements OnInit {



  selectedCity: City;

  cities:City[] = [
    {name: 'New York', code: 'NY'},
    {name: 'Rome', code: 'RM'},
    {name: 'London', code: 'LDN'},
    {name: 'Istanbul', code: 'IST'},
    {name: 'Paris', code: 'PRS'}
];


  constructor() { }

  ngOnInit(): void {
  }

}
