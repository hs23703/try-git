import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TryDropDownComponent } from './try-drop-down.component';

describe('TryDropDownComponent', () => {
  let component: TryDropDownComponent;
  let fixture: ComponentFixture<TryDropDownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TryDropDownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TryDropDownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
